*[Home](../../home) > [Features](../../features) &gt;* Reorder timelines

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Reorder Timelines
With this feature, you can customize tabs in Fedilab, the way you want.

To open the reorder page, open the navigation drawer from the side and select "Reorder timelines". Then you should see a page like this with similar content.

 <img src="../../res/reorder_timelines/reorder_page.png" width="300px">

The items in this list are the tabs you see when you open Fedilab. Here you can manage them. There are different types of timelines in this list.

#### Timeline types

- **Main/default timelines**<br>
&nbsp;&nbsp;&nbsp;*These are the tabs that comes with Fedilab. You can hide them if you want. But they cannot be completely removed from the list. These tabs are*
  - Home
  - Notifications
  - Direct messages
  - Local timeline
  - Federated timeline
  - [Art timeline](../art-timeline)
  - Peertube
- **Pinned tag Timelines**<br>
&nbsp;&nbsp;&nbsp;*These are the \# tags you have pinned. To pin a tag you have to simply use the search feature to view a tag and press + sign on top*
- **Followed instances**<br>
&nbsp;&nbsp;&nbsp;*With Fedilab, You can have a public timelines of other instances as tabs. For example if you want to see the public images of pixelfed.social, you can add it to Fedilab as another timeline. Cool, isn't it?<br>[How to add an instance](#add-instance)*
- **Lists**<br>
&nbsp;&nbsp;&nbsp;*These are the lists you have created in your account*
<br><br>

#### Manage timelines
- **Reorder**<br>
&nbsp;&nbsp;&nbsp;*To change the order of tabs you can drag each item by its handle or long press and drag from anywhere on an item.*<br><br>
<img src="../../res/reorder_timelines/reorder.gif" width="500px">

- **Remove**<br>
&nbsp;&nbsp;&nbsp;*To remove an item from the list you just have to swipe it. You can swipe to remove lists, followed instances and pinned tags. However main/default timeline cannot be removed from the list. You can only hide them.* **If you removed a tab you want by mistake, don't worry. You have the chance to undo the last action for few seconds.**<br><br>
<img src="../../res/reorder_timelines/remove_undo.gif" width="500px">

- **Hide**<br>
&nbsp;&nbsp;&nbsp;*If you just want to hide a timeline without removing it, use the eye icon*<br><br>
<img src="../../res/reorder_timelines/hide.png" width="500px">

<a name="add-instance"></a><br>
<h4>How to add an instance</h4>
1. Press the + icon on the top in this screen<br><br>
<img src="../../res/reorder_timelines/add_instance_1.png" width="300px">
<br>
2. Type the address of the instance you want to add and select type. Then press validate<br><br>
<img src="../../res/reorder_timelines/add_instance_2.png" width="300px">
